import React, { Component } from 'react'
import Welcome from './components/Users/Welcome'
import './App.css'
import Homepage from './components/Landing/Homepage'
import { BrowserRouter, Route } from 'react-router-dom'
import RestaurantListPage from './components/RestaurantListPage/RestaurantListPage'
import RestaurantPage from './components/RestaurantPage/RestaurantPage'
import Checkout from './components/checkout/Checkout'


export class App extends Component {
  render() {
    return (
      <BrowserRouter>

        <Route path='/' exact component={Homepage} />
        <Route path='/checkout' component={Checkout} />
        <Route path='/user' component={Welcome} />
        <Route exact path='/restaurant' component={RestaurantListPage} />
        <Route path='/restaurant/:url' component={RestaurantPage} />

      </BrowserRouter>
    )
  }
}

export default App
