import { Grid } from '@material-ui/core';
import React from 'react';
import Header from '../Header/Header';

import axios from 'axios'
import RestaurantCard from './RestaurantCard';
import { withRouter } from 'react-router-dom';

function RestaurantListPage({match}) {
    const [restaurants, setRestaurants] = React.useState([])

    React.useEffect(() => {
        async function fetchData() {
            try{
                const resResult = await axios.get('https://zomato-mangal-saurabh.herokuapp.com/restaurants')
                await setRestaurants(resResult.data.data)
            }catch(error){
                console.log(error, 'error')
            }
        }
        if (restaurants.length === 0) {
            fetchData()
        }
    }, [restaurants])
    return (
        <Grid container justify="center">
            <Grid container item lg={12} style={{}}>
                <Header />
       </Grid>
            <Grid container item lg={8}>
                <Grid container style={{fontSize: "1.6em", padding: '10px'}}>
                    <span>Delivery Restaurants near you</span>
                </Grid>

                <Grid item lg={12} container>
                    {restaurants.map((restaurant, index) => {
                        return (
                            <RestaurantCard key={index} {...restaurant} /> 
                        )
                    })}
                </Grid>
            </Grid>
        </Grid>
    )
}

export default withRouter(RestaurantListPage)