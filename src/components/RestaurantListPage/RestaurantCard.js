import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import { Link, withRouter } from 'react-router-dom';

function restaurantCard({ name, imageUrl, address, url, match }) {
    return (
        <Grid item lg={4}>
            <Grid container alignItems="center" direction="column" >
                <Grid item>
                    <img
                        src={imageUrl}
                        alt={"restaurant pic"}
                        style={{
                            height: '230px',
                            width: '280px',
                            borderRadius: '8px',
                            margin: '10px'
                        }}
                    />
                </Grid>
                <Grid item container
                    style={{
                        paddingLeft: '25px'
                    }}
                >
                    <Grid container direction="column" justify="flex-start" style={{}}>
                        <Link to={`${match.url}/${url}`} style={{color: 'black', fontSize: '1.4em'}} >{name}</Link>
                        <Typography style={{ fontSize: '0.8em', color: 'gray' }}>{address}</Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default withRouter(restaurantCard)
