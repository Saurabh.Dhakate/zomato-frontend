import React from 'react'
import { Link, Route, withRouter } from 'react-router-dom'

import axios from 'axios'

import PropTypes from 'prop-types'

import { connect } from 'react-redux'

import { createStructuredSelector } from 'reselect'

import toggleCartDropdown from '../../redux/cart/cartAction'
import { selectCartItems, selectCartHidden, selectCartItemsCount, selectSubTotal } from '../../redux/cart/cartSelector'
import ItemCollection from './ItemCollection'
import Cart from './Cart'
import Overview from './overview/Overview'
import Reviews from './reviews/Reviews'

import { Grid, Typography, Button } from '@material-ui/core'
import Rating from '@material-ui/lab/Rating'
import { withStyles } from '@material-ui/core/styles'
import FavoriteIcon from '@material-ui/icons/Favorite'
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied'
import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentDissatisfied'
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied'
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAltOutlined'
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'


function RestaurantPage({ cartItems, cartHidden, cartItemsCount, subTotal, dispatch, match, history }) {

    const [dataObj, setDataObj] = React.useState({})
    const [categories, setCategories] = React.useState([])
    const [restaurant, setRestaurant] = React.useState({})
    const url = match.params.url

    React.useEffect(() => {
        async function fetch(url) {
            try{
                const res = await axios.get(`https://zomato-mangal-saurabh.herokuapp.com/restaurants/${url}`)
                setRestaurant(res.data.data)

                const resResult = await axios.get(`https://zomato-mangal-saurabh.herokuapp.com/restaurants-details/${res.data.data.id}`)
                const data = resResult.data.data.reduce((acc, item) => {
                    if (acc[item.category]) {
                        acc[item.category].push({
                            id: item.id,
                            imageUrl: item.imageUrl,
                            name: item.name,
                            price: item.price,
                        })
                    } else {
                        acc[item.category] = [{
                            id: item.id,
                            imageUrl: item.imageUrl,
                            name: item.name,
                            price: item.price,
                        }]
                    }
                    return acc
                }, {})
    
                setDataObj(data)
                setCategories(Object.keys(data))
            }catch(error){
                console.log(error, "error")
            }

        }
        fetch(url)
    }, [url])

    const StyledRating = withStyles({
        iconFilled: {
            color: '#ff6d75',
        },
        iconHover: {
            color: '#ff3d47',
        },
    })(Rating)

    const customIcons = {
        1: {
            icon: <SentimentVeryDissatisfiedIcon />,
            label: 'Very Dissatisfied',
        },
        2: {
            icon: <SentimentDissatisfiedIcon />,
            label: 'Dissatisfied',
        },
        3: {
            icon: <SentimentSatisfiedIcon />,
            label: 'Neutral',
        },
        4: {
            icon: <SentimentSatisfiedAltIcon />,
            label: 'Satisfied',
        },
        5: {
            icon: <SentimentVerySatisfiedIcon />,
            label: 'Very Satisfied',
        },
    }

    function IconContainer(props) {
        const { value, ...other } = props
        return <span {...other}>{customIcons[value].icon}</span>
    }

    IconContainer.propTypes = {
        value: PropTypes.number.isRequired,
    }

    return (
        <div>
            {/* <Header /> */}
            <Grid container lg={12} item={true} justify="center">
                <Grid container item lg={10} md={10} xs={11} justify="center" direction="column" style={{ height: '100%'}}>
                    <Grid>
                        <Grid item xs={12} style={{ height: '350px', width: '100%', cursor: 'pointer' }}>
                            <img src={restaurant.imageUrl}
                                alt="restaurant pic"
                                height={'100%'}
                                width={'100%'}
                            />
                        </Grid>

                        <Grid container direction="row" justify="space-between" style={{ paddingLeft: '20px', position: 'sticky', top: '0' }}>
                            <Grid item xs={6}>
                                <h1>{restaurant.name}</h1>
                                <p>{restaurant.address}</p>
                            </Grid>

                            <Grid item container xs={4} justify="flex-end" style={{ paddingTop: '10px' }}>
                                <Grid container justify="space-around" style={{ marginTop: '10px' }}>
                                    <Grid >
                                        <Grid container>
                                            <Rating
                                                name="customized-icons"
                                                defaultValue={2}
                                                getLabelText={(value) => customIcons[value].label}
                                                IconContainerComponent={IconContainer}
                                            />

                                            <Typography>4.4</Typography>
                                        </Grid>
                                        <Typography style={{ borderBottom: 'dashed 1px gray' }}>Deliveries Reviews</Typography>
                                    </Grid>

                                    <Grid>
                                        <Grid container>
                                            <StyledRating
                                                name="customized-color"
                                                defaultValue={2}
                                                getLabelText={(value) => `${value} Heart${value !== 1 ? 's' : ''}`}
                                                precision={0.5}
                                                icon={<FavoriteIcon fontSize="inherit" />}
                                            />

                                            <Typography > 4.3</Typography>
                                        </Grid>

                                        <span style={{ borderBottom: '1px dashed rgb(181, 181, 181)' }}>Dining Reviews</span>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid style={{ margin: '10px', fontSize: '1.5em', color: 'gray' }}>
                        <Grid container direction="row" justify="space-around" className="intro" style={{ borderBottom: '2px solid gray' }}>
                            <Grid><Link to={`/restaurant/${match.params.url}/overview`}>Overview</Link></Grid>

                            <Grid style={{ color: '#6b5aed', margin: '0px' }} >
                                <Link to={`/restaurant/${match.params.url}`} style={{ color: '#6b5aed', borderBottom: '4px solid #6b5aed', height: '10px' }}>
                                    Order Online
                                </Link>
                            </Grid>

                            <Grid><Link to={`/restaurant/${match.params.url}/reviews`}>Reviews</Link></Grid>
                        </Grid>
                    </Grid>

                    <Route exact path={`/restaurant/${match.params.url}`}>
                        <Grid justify="center" container direction="row" style={{}}>
                            <Grid container item direction="column" justify="flex-start" xs={3} style={{ borderRight: '1px solid gray', paddingLeft: "20px" }}>
                                {
                                    categories.map((category, id) => {
                                        return <Grid style={{ padding: '10px', borderBottom: '2px solid gray' }} key={id}><Link to={`#`}>{category}</Link></Grid>
                                    })
                                }
                            </Grid>

                            <Grid item xs={8} style={{overflow: 'scroll', height: '600px' }}>

                                {
                                    categories.map((category, id) => {
                                        return <ItemCollection items={dataObj[category]} key={id} itemCategory={category}/>
                                    })
                                }
                                
                            </Grid>
                        </Grid>
                    </Route>

                    <Route exact path={`/restaurant/${match.params.url}/reviews`}><Reviews /></Route>

                    <Route exact path={`/restaurant/${match.params.url}/overview`}><Overview /></Route>
                </Grid>
            </Grid>
            {
                cartItems.length > 0 ?
                    <Grid
                        container
                        className="drawer"
                        justify="center"
                        style={{
                            position: 'fixed',
                            zIndex: '11',
                            bottom: '0px',
                            transition: 'bottom 0.2s ease 0s',
                            width: '100%',
                            height: '6.5rem',
                            background: 'rgb(255, 255, 255)'
                        }}
                    >
                        <Grid item lg={8} container justify="center" alignItems="center">
                            <Grid container justify="space-between" style={{ marginTop: '40px' }}>
                                <Grid container item lg={4} justify="center" >
                                    <Grid item lg={2} >
                                        <Button onClick={() => dispatch(toggleCartDropdown())} >
                                            {cartHidden ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />}
                                        </Button>
                                    </Grid>

                                    <Grid item lg={4} container alignItems="center" justify="center">
                                        Items({cartItemsCount})
                                    </Grid>
                                </Grid>

                                <Grid xs={4} item >
                                    <Grid container justify="space-around" alignItems="center">
                                        <Grid item style={{ fontSize: '1.2em' }}>
                                            Subtotal: Rs. {subTotal}
                                        </Grid >

                                        <Grid item>
                                            <Link to="/checkout">
                                                <Button variant="contained" color="secondary">Continue</Button>
                                            </Link>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    : null
            }

            {cartHidden && cartItems.length > 0 ? <Cart /> : null}

        </div >

    )
}


const mapStateToProps = createStructuredSelector({
    cartItems: selectCartItems,
    cartHidden: selectCartHidden,
    cartItemsCount: selectCartItemsCount,
    subTotal: selectSubTotal
})


export default connect(mapStateToProps)(withRouter(RestaurantPage))