import React from 'react'

import { Grid } from '@material-ui/core'

import ItemCard from './ItemCard'

export default function ItemCollection({ itemCategory, items }) {
  
  return (
    <Grid container direction='column' item xs={12} style={{ fontSize: '1.4em', marginTop: '10px'}}>
      <div style={{ fontSize: '1.4em', paddingLeft: '10px', height: '40px', borderBottom: '1px solid gray', position: 'sticky', top: '0', backgroundColor: 'white', zIndex: '3' }}> {itemCategory}</div>
      {
        items.map((item, id)=>{
          return <ItemCard key={id} item={item} />
        })
      }
    </Grid>
  )
}
