import React from "react"

import { connect } from 'react-redux'

import { createStructuredSelector } from 'reselect'

import Button from "@material-ui/core/Button"
import ButtonGroup from "@material-ui/core/ButtonGroup"

import { addItemToCart, removeItemFromCart } from '../../redux/cart/cartAction'
import { selectCartItems } from '../../redux/cart/cartSelector'


function IncrementDecrement({ addItemToCart, removeItemFromCart, item, cartItems }) {

  const existingItem = cartItems.find((cartItem) => {
    return cartItem.id === item.id
  })

  return (
    <div key={item.id}>
      {
        existingItem ?
          <ButtonGroup size="small" aria-label="small outlined button group">
            <Button onClick={() => { removeItemFromCart(item) }}>-</Button>
            <Button disabled>{ item.quantity||'Add'}</Button>
            <Button onClick={() => { addItemToCart(item) }}>+</Button>
          </ButtonGroup> :

          <ButtonGroup size="small" aria-label="small outlined button group">
            <Button disabled>Add</Button>
            <Button onClick={() => { addItemToCart(item) }}>+</Button>
          </ButtonGroup>
      }


    </div>
  )
}

const mapStateToProps = createStructuredSelector({
  cartItems: selectCartItems
})

const mapDispatchToProps = (dispatch) => ({
  addItemToCart: (item) => dispatch(addItemToCart(item)),
  removeItemFromCart: (item) => dispatch(removeItemFromCart(item)),
})

export default connect(mapStateToProps, mapDispatchToProps)(IncrementDecrement)