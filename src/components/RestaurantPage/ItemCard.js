import React from 'react'

import { Grid, Typography, } from '@material-ui/core'
import Rating from '@material-ui/lab/Rating'

import IncrementDecrement from './IncrementDecrement'

export default function ItemCard({item}) {
    const [value, setValue] = React.useState(2)
    return (
        <Grid container style={{ paddingLeft: '10px', height: '130px', marginTop: '10px' }}>

            <img
                style={{ borderRadius: '6px' }}
                alt="pic item"
                height={130}
                width={130}
                src={item.imageUrl}
            />
            <Grid xs={8} item container direction="column" style={{ paddingLeft: '10px' }}>
                <span>{item.name}</span>
                <Grid container alignItems="center" style={{ marginTop: '10px' }}>
                    <Rating
                        name="simple-controlled"
                        value={value}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                    />
                    <Typography component="p" style={{ fontSize: '14px', padding: '5px', color: 'gray' }} >480 votes</Typography>

                </Grid>

                <span style={{ fontSize: '16px', padding: '5px' }}>Rs. {item.price}</span>
                <p style={{ fontSize: '16px', margin: '0', color: 'gray' }}>
                    Mildly spicy in taste made with succulent chicken pieces layered with long grain basmati rice and 7 secret spices.
                </p>
            </Grid >
            
            <Grid xs={2} item container justify="center" alignItems="center" >
                <IncrementDecrement item={item} />
            </Grid>
        </Grid>
    )
}
