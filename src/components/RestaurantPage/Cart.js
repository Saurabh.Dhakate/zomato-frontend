import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { selectCartItems } from '../../redux/cart/cartSelector'
import toggleCartDropdown from '../../redux/cart/cartAction'
import CartItem from "./cartItem"

import { Grid } from '@material-ui/core'


function Cart({ cartItems, dispatch }) {
    return (
        <Grid container justify="center"
            style={{
                position: 'fixed',
                bottom: "60px",
                backgroundColor: "white",
                zIndex: '11'
            }}
        >
            <Grid container item lg={10} justify="center">
                <Grid item xs={8} style={{}}>
                    <Grid container justify="space-between" style={{padding:"15px", borderBottom: '1px solid gray'}}>
                        <Grid item>Your Orders</Grid>
                        <Grid onClick={()=>dispatch(toggleCartDropdown())} style={{cursor: 'pointer', paddingRight: "20px"}} item>X</Grid>
                    </Grid>

                    <Grid
                        container
                        item
                        xs={12}
                        style={{
                            height: "240px",
                            overflow: "scroll"
                        }}
                    >
                        {
                            cartItems.map((cartItem) => {
                                console.log(cartItem.id)
                                return <CartItem key={cartItem.id} item={cartItem} />
                            })
                        }
                    </Grid>
                </Grid>
            </Grid>
        </Grid >
    )
}

const mapStateToProps = createStructuredSelector({
    cartItems: selectCartItems
})

export default connect(mapStateToProps)(Cart)