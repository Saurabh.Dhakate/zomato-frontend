import { Grid } from '@material-ui/core';
import React from 'react';
import IncrementDecrement from './IncrementDecrement'

export default function cartItem({ item }) {
  const { imageUrl, price, name, quantity } = item
  return (
    <Grid item container justify="center" className="cart-item" style={{margin: "5px" }}>
      <Grid container justify="flex-start" alignItems="center" item xs={8} >
        <Grid item>
          <img
            src={imageUrl}
            alt="item"
            height="80px"
            width="80px"
            style={{ borderRadius: "10px" }}
          />
        </Grid>

        <Grid xs={10} item className="item-details" >
          <Grid xs={12} item direction="row" container justify="space-between" >
            <Grid item>
              <Grid container direction="column" style={{ paddingLeft: "10px" }}>
                <span className="name">{name}</span>
                <span className="price">
                  Rs.{price} x {quantity} = Rs.{price*quantity}
                </span>
              </Grid>
            </Grid>

            <Grid style={{ float: 'right' }}>
              <IncrementDecrement item={item} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}
