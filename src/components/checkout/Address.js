import { Button, Card } from '@material-ui/core'
import MapPicker from 'react-google-map-picker'
import React from 'react'

export default function Address() {

    return (

        <Card style={{ marginTop: '10px' }}>
            <h3 style={{ margin: '10px 5%' }}>Address</h3>
            {(localStorage.getItem('user') ? <div style={{
                width: '90%',
                padding: '10px 5%',
            }}>
                <MapPicker defaultLocation={{ lat: 18.5204, lng: 73.8567 }}
                    zoom={10}
                    style={{ height: '300px' }}
                    apiKey='AIzaSyBK9RCFe2RMVyscZjIiSviIyqahlJ8EpXE' />
                <Button fullWidth color='secondary'>+ Add Address</Button>
            </div> : <div style={{ width: '90%', padding: '10px 5%' }}>
                    <h5 style={{ fontWeight: 'lighter', color: 'grey' }}>Login to enter address</h5>
                </div>)
            }
        </Card >
    )
}