import { Button, Card } from '@material-ui/core'
import React from 'react'
import { useSelector } from 'react-redux'
import CartItem from './CartItem'

export default function Cart() {
    let cartItems = useSelector(state => state.cart.cartItems)
    let grandTotal = cartItems.map(item => (item.price * item.quantity)).reduce((a, b) => a + b, 0)

    return (
        <>
            <h3>Summary</h3>
            <Card>
                <div style={{ width: '90%', padding: '10px 5%', backgroundColor: '#e9e9e9' }}>
                    <h2 style={{ marginTop: '0' }}>Ordered From</h2>
                    <p>Restaurants Name</p>
                </div>
                {cartItems.map(item => <CartItem item={item} />)}
                <div style={{ width: '90%', padding: '10px 5%', backgroundColor: '#e9e9e9', display: 'flex',justifyContent:'space-between', alignItems:'center' }}>
                    <h4>Grand Total - ₹ {grandTotal}</h4>
                    <div>
                        <Button disabled={!localStorage.getItem('user')} variant='contained' color='primary'>Proceed</Button>
                    </div>
                </div>
            </Card>
        </>
    )
}
