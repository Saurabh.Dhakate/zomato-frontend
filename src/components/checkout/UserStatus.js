import { Button, Card } from '@material-ui/core'
import React from 'react'
import Alert from '@material-ui/lab/Alert'
import { useHistory } from 'react-router-dom'

export default function UserStatus() {
    let history = useHistory()
    return (
        <Card >
            {(localStorage.getItem('user') ? <div style={{ width: '90%', padding: '10px 5%' }}>
                <h2 style={{ marginTop: '0' }}>{localStorage.getItem('user')}</h2>
                <Alert>You are securely logged in</Alert>
            </div> : <div style={{ width: '90%', padding: '10px 5%' }}>
                    <h3 style={{ marginTop: '0' }}>Account</h3>
                    <h5 style={{ fontWeight: 'lighter', color: 'grey' }}>It’s easy to login or create an account with Good-Food</h5>
                    <Button variant='outlined' color='secondary' onClick={()=>{history.push('/user/login')}} style={{ width: '45%', marginRight: '10%' }}>Login</Button>
                    <Button variant='contained' color='secondary' onClick={()=>{history.push('/user/register')}} style={{ width: '45%' }}>Signup</Button>
                </div>)}
        </Card>
    )
}
