import { Fab, Grid } from '@material-ui/core'
import React, { useState } from 'react'
import Account from '../Landing/Account'
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import UserStatus from './UserStatus'
import Address from './Address'
import Cart from './Cart';
import { useHistory } from 'react-router-dom';

export default function Checkout() {
    let history = useHistory()
    let [menu, setMenu] = useState(false)
    return (
        <Grid container justify='center' style={{ height: '100px' }}>

            <div style={{ width: '95%', padding: '0 2.5%' , position:'relative'}}>
                <Grid item xs={12} style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <h2 style={{ fontFamily: 'Lobster', textAlign: 'center', fontSize: '40px' }}>Good - Food</h2>
                    <MenuOpenIcon color='primary' style={{ fontSize: '40px', margin: 'auto 0', cursor: 'pointer' }} onClick={() => { setMenu(!menu) }} />
                </Grid>
                {(menu) ? <div style={{ position: 'absolute', right: "2.5%", top: '70px' }}>
                    <Account />
                    <Fab onClick={()=>history.goBack()} variant='extended' style={{ marginTop: '78px', marginRight: '2.5%', zIndex: '0' }}> Back</Fab>
                </div> : null}
            </div>
            <Grid container justify='center'>
                <Grid item md={6} lg={8} xs={10}>
                    <div style={{ width: '95%', padding: '0 2.5%' }}>
                        <UserStatus />
                        <Address />
                    </div>
                </Grid>
                <Grid item md={6} lg={4} xs={10}>
                    <div style={{ width: '95%', padding: '0 2.5%' }}>
                        <Cart />
                        
                    </div>
                </Grid>
            </Grid>
        </Grid>
    )
}
