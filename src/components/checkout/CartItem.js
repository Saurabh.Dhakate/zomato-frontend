import { Card } from '@material-ui/core'
import React from 'react'
import IncrementDecrement from '../RestaurantPage/IncrementDecrement'

export default function CartItem(props) {
    return (
        <Card>
            <div style={{ width: '90%', padding: '0px 5%', display: 'flex', justifyContent:'space-between',alignItems:'center' }}>
                <img src={props.item.imageUrl} alt="" style={{width:'60px'}}/>
                <div style={{ marginLeft:'10px' ,display: 'flex',width:'100%', minHeight: '75px', flexDirection: 'column', justifyContent: 'center' }}>
                    <h4 style={{ margin: '5px 0' }}>{props.item.name}</h4>
                    <h6 style={{ margin: '0 5px 0 0',color:'grey' }}>₹ {props.item.price}</h6>
                    <h6 style={{ margin: '5px 5px 0 0' }}>Total - ₹ {props.item.price*props.item.quantity}</h6>
                </div>
                <IncrementDecrement item={props.item} />
            </div>
        </Card >
    )
}
