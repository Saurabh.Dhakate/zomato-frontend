import React, { Component } from 'react'
import Login from './Login'
import Register from "./Register";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import { Grid } from '@material-ui/core'
import Otp from './Otp';

export class Welcome extends Component {

    render() {
        return (
            <Grid container xs={12} justify='center'>
                <Grid container lg={8} md={10} xs={11} className='main-div-welcome' >
                    <div className='brand-info'>
                        <h1 style={{fontFamily:'Lobster', fontSize:'55px'}}>Good - Food</h1>
                        <img src="https://www.flaticon.com/svg/static/icons/svg/45/45332.svg" alt="img-brand-info"
                            style={{
                                height: '50%',
                                filter: 'invert(1) drop-shadow(2px 4px 6px black)'
                            }} />
                    </div>
                    <div className='login-form'>
                        <div className='login-brand-info'>
                            <h1 style={{fontFamily:'Lobster'}}>Good - Food</h1>
                            <img src="https://www.flaticon.com/svg/static/icons/svg/45/45332.svg" alt="img-logon-form"
                                style={{
                                    height: '30%',
                                    marginLeft: '20px'
                                }} />
                        </div>
                        <BrowserRouter>
                            <Route path='/user' exact ><Redirect to='/user/login' /></Route>
                            <Route path='/user/login'  ><Login /></Route>
                            <Route path='/user/register' ><Register /></Route>
                            <Route path='/user/otp' ><Otp /></Route>
                        </BrowserRouter>
                    </div>
                </Grid>
            </Grid>
        )
    }
}

export default Welcome
