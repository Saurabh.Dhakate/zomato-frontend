import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Button, TextField, Grid } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useDispatch, useSelector } from 'react-redux'
import Joi from 'joi'
import axios from 'axios'

export default function Register() {
    let dispatch = useDispatch()
    let [loading, setLoading] = useState(false)
    let history = useHistory()
    let err_msg = useSelector(state => state.signup.err_msg)
    let error = useSelector(state => state.signup.error)
    let err_msg_fname = useSelector(state => state.signup.err_msg_fname)
    let error_fname = useSelector(state => state.signup.error_fname)
    let err_msg_lname = useSelector(state => state.signup.err_msg_lname)
    let error_lname = useSelector(state => state.signup.error_lname)
    let err_msg_pass = useSelector(state => state.signup.err_msg_pass)
    let error_pass = useSelector(state => state.signup.error_pass)
    let err_msg_repass = useSelector(state => state.signup.err_msg_repass)
    let error_repass = useSelector(state => state.signup.error_repass)
    let password = useSelector(state => state.signup.password)
    let email = useSelector(state => state.signup.email)
    let repassword = useSelector(state => state.signup.repassword)
    let fname = useSelector(state => state.signup.fname)
    let signup_err_msg = useSelector(state => state.signup.signup_err_msg)

    const emailSchema = Joi.object({
        email_id: Joi.string().email({ tlds: { allow: false } }),
    })
    const passSchema = Joi.object({
        password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    })
    const nameSchema = Joi.object({
        name: Joi.string().pattern(new RegExp('^[a-zA-Z]{3,30}$'))
    })


    let validated_email = email => emailSchema.validate({ email_id: email })
    let validated_pass = pass => passSchema.validate({ password: pass })
    let validated_name = name => nameSchema.validate({ name: name })
    let validated_repass = repass => (repass !== password)

    let disableBtn = loading || validated_repass(repassword) || validated_email(email).error || validated_name(fname).error || validated_pass(password).error

    let handleEmailChange = event => dispatch({
        type: 'update/email',
        payload: event.target.value,
        error: (validated_email(event.target.value).error) ? true : false,
        err_msg: (validated_email(event.target.value).error) ? 'Invalid Email' : null
    })

    let handleFnameChange = event => dispatch({
        type: 'update/fname',
        fname: event.target.value,
        error_fname: (validated_name(event.target.value).error) ? true : false,
        err_msg_fname: (validated_name(event.target.value).error) ? 'Invalid First Name' : null
    })

    let handleLnameChange = event => dispatch({
        type: 'update/lname',
        lname: event.target.value,
        error_lname: (validated_name(event.target.value).error) ? true : false,
        err_msg_lname: (validated_name(event.target.value).error) ? 'Invalid Last Name' : null
    })

    let handlePassChange = event => dispatch({
        type: 'update/pass',
        payload: event.target.value,
        error_pass: (validated_pass(event.target.value).error) ? true : false,
        err_msg_pass: (validated_pass(event.target.value).error) ? 'Invalid Password' : null
    })

    let handleRePassChange = event => dispatch({
        type: 'update/repass',
        repassword: event.target.value,
        error_repass: validated_repass(event.target.value),
        err_msg_repass: (validated_repass(event.target.value)) ? 'Invalid Re-Password' : null
    })

    const generateOTP = () => {
        let otp = ""
        for (let count = 0; count < 6; count++) {
            otp += Math.floor(Math.random() * 9 + 1)
        }
        return otp
    }

    let handleClick = () => {
        setLoading(true)
        axios.post('https://zomato-mangal-saurabh.herokuapp.com/check-email', {
            email: email
        }).then(res => {
            setLoading(false)
            if (res.data.status === "error") {
                dispatch({ type: 'update/signup_err', signup_err_msg: res.data.message })
            }
            else {
                let otp = generateOTP()
                dispatch({ type: 'update/otp', otp: otp })
                axios.post('https://zomato-mangal-saurabh.herokuapp.com/sendOtp', {
                    email: email,
                    message: `Your account verification OTP is ${otp}`
                }).then(() => {
                    localStorage.setItem('otp', otp)
                    history.push('/user/otp')
                }).catch(() => {
                    setLoading(false)
                    dispatch({ type: 'update/signup_err', signup_err_msg: "Something went wrong" })
                })

            }
        })
    }



    return (
        <Grid container justify='center' style={{ height: '100%' }} alignItems='flex-start' alignContent='flex-start'>

            <Grid item lg={6} xs={10} style={{ height: '20%' }} >
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', height: 'auto' }}>
                    <Link to="/user/login" style={{ textDecoration: 'none', color: 'grey', width: '40%' }}>
                        <div style={{ paddingBottom: '2px' }}>
                            <h3 style={{ textAlign: 'center' }}>Login</h3>
                        </div>
                    </Link>
                    <Link to="/user/register" style={{ textDecoration: 'none', color: '#494e55', width: '40%' }}>
                        <div style={{ borderBottom: '2px grey solid' }}>
                            <h3 style={{ textAlign: 'center' }}>Register</h3>
                        </div>
                    </Link>
                </div>
            </Grid>

            <Grid item lg={8} xs={10} style={{ height: '70%', margin: '0' }} >

                <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', height: '100%' }}>
                    {(signup_err_msg) ? <Alert severity='error' style={{ width: '100%', marginBottom: '10px', padding: '0 6px' }}>{signup_err_msg}</Alert> : null}
                    <TextField helperText={err_msg} error={error} label='Email' style={{ width: '100%', }} onChange={(e) => handleEmailChange(e)}></TextField>
                    <TextField helperText={err_msg_fname} error={error_fname} label='First Name' style={{ width: '45%', }} onChange={(e) => handleFnameChange(e)}></TextField>
                    <TextField helperText={err_msg_lname} error={error_lname} label='Last Name' style={{ width: '45%', }} onChange={(e) => handleLnameChange(e)}></TextField>
                    <TextField helperText={err_msg_pass} error={error_pass} type='password' label='Password' style={{ width: '45%', }} onChange={e => handlePassChange(e)}></TextField>
                    <TextField helperText={err_msg_repass} error={error_repass} type='password' label='Re-Password' style={{ width: '45%' }} onChange={e => handleRePassChange(e)}></TextField>
                    <div style={{ position: 'relative', width: '100%' }}>
                        <Button type='submit' disabled={disableBtn} variant='contained' color='primary' style={{ width: '100%', marginTop: '15px' }} onClick={handleClick}>
                            Register
                         </Button>
                        {loading ? <CircularProgress size={30} style={{ position: 'absolute', top: '18px', right: 0, left: 0, margin: 'auto' }} /> : null}
                    </div>
                </div>
            </Grid>
        </Grid>

    )

}
