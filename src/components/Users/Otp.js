import React from 'react'
import { Button, Grid, TextField } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import axios from 'axios'

export default function Otp() {
    let dispatch = useDispatch()
    let fname = useSelector(state => state.signup.fname)
    let lname = useSelector(state => state.signup.lname)
    let email = useSelector(state => state.signup.email)
    let password = useSelector(state => state.signup.password)
    let otp = useSelector(state => state.signup.otp)
    let userOtp = useSelector(state => state.otp.userOtp)
    let otp_err = useSelector(state => state.otp.otp_err)

    let handlechange = (event) => {
        dispatch({ type: 'update/userOtp', userOtp: event.target.value })
    }

    let validate_otp = () => {
        if (userOtp.length !== 6) {
            return true
        }
        else {
            return false
        }
    }

    let handleSubmit = () => {
        if (otp === userOtp) {
            axios.post('https://zomato-mangal-saurabh.herokuapp.com/signup', {
                firstName: fname,
                lastName: lname,
                email: email,
                password: password,
            }).then(() => {
                window.location = '/'
            })
                .catch(console.log)
        }
        else {
            dispatch({ type: 'update/otp_err', otp_err: 'Invalid OTP' })
        }
    }

    return (
        <Grid container lg={8} xs={10} direction='column' alignItems='center' justify='center'>
            <h3>Verify 6 Digit OTP</h3>
            {(otp_err) ? <p style={{ margin: '0', color: 'red' }}>{otp_err}</p> : null}
            <TextField label='OTP' style={{ width: '100%' }} helperText='OTP has been sent to your registered email' onChange={e => handlechange(e)}></TextField>
            <Button
                type='submit'
                disabled={validate_otp()}
                onClick={handleSubmit}
                variant='contained'
                color='primary'
                style={{
                    width: '100%',
                    marginTop: '100px'
                }}
            >
                Submit
                    </Button>
        </Grid>
    )
}
