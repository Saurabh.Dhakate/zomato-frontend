import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Button, Grid, TextField } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { useDispatch, useSelector } from 'react-redux'
import Joi from 'joi'
import axios from 'axios'
import CircularProgress from '@material-ui/core/CircularProgress'


export default function Login() {
    const dispatch = useDispatch()
    let history = useHistory()
    let email = useSelector(state => state.login.email)
    let password = useSelector(state => state.login.password)
    let err_msg = useSelector(state => state.login.err_msg)
    let error = useSelector(state => state.login.error)
    let err_msg_pass = useSelector(state => state.login.err_msg_pass)
    let error_pass = useSelector(state => state.login.error_pass)
    let login_err_msg = useSelector(state => state.login.login_err_msg)
    let [loading, setLoading] = useState(false)


    const emailSchema = Joi.object({
        email_id: Joi.string().email({ tlds: { allow: false } }),
    })
    const passSchema = Joi.object({
        password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    })


    let validated_email = email => emailSchema.validate({ email_id: email })
    let validated_pass = pass => passSchema.validate({ password: pass })


    let handleEmailChange = (event) => {
        dispatch({
            type: 'update/email',
            payload: event.target.value,
            error: (validated_email(event.target.value).error) ? true : false,
            err_msg: (validated_email(event.target.value).error) ? 'Invalid Email' : null
        })


    }
    let handlePassChange = (event) => {
        dispatch({
            type: 'update/pass',
            payload: event.target.value,
            error_pass: (validated_pass(event.target.value).error) ? true : false,
            err_msg_pass: (validated_pass(event.target.value).error) ? 'Invalid Password' : null
        })
        dispatch({ type: 'loggedin', payload: { login_msg: null, login_err_msg: null } })

    }

    let handleClick = () => {
        setLoading(true)
        axios.post('https://zomato-mangal-saurabh.herokuapp.com/login', {
            email: email,
            password: password
        }).then((response) => {
            setLoading(false)
            if (response.data.token) {
                dispatch({ type: 'loggedin', payload: { login_msg: response.data.message, login_err_msg: null } })
                localStorage.setItem('email', response.data.user.email)
                localStorage.setItem('user', response.data.user.firstName)
                localStorage.setItem('token', response.data.token)
                console.log(localStorage.getItem('login_from').includes('checkout'))
                if(localStorage.getItem('login_from').includes('checkout')){
                    history.push('/checkout')
                    history.goBack()
                    history.goForward()
                }
                else{
                    history.push('/')
                    history.go(0)
                }
            }
            else {
                console.log("object")
                dispatch({ type: 'loggedin', payload: { login_msg: null, login_err_msg: response.data.message } })
            }
        })
            .catch(() => {
                setLoading(false)
                dispatch({ type: 'loggedin', payload: { login_msg: null, login_err_msg: "Something went wrong, Please try again" } })
            })
    }

    return (
        <Grid container justify='center' style={{ height: '100%' }} alignItems='flex-start' >

            <Grid item lg={6} xs={10} style={{ height: '20%' }} >

                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', height: 'auto' }}>
                    <Link to="/user/login" style={{ textDecoration: 'none', color: '#494e55', width: '40%' }}>
                        <div style={{ borderBottom: '2px grey solid' }}>
                            <h3 style={{ textAlign: 'center' }}>Login</h3>
                        </div>
                    </Link>
                    <Link to="/user/register" style={{ textDecoration: 'none', color: 'grey', width: '40%' }}>
                        <div style={{ paddingBottom: '2px' }}>
                            <h3 style={{ textAlign: 'center' }}>Register</h3>
                        </div>
                    </Link>
                </div>
            </Grid>

            <Grid item lg={8} xs={10} style={{ height: '60%' }} >

                <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly', height: '100%' }}>
                    {(login_err_msg) ? <Alert severity="error" style={{ marginBottom: '10px', padding: '0 6px' }}>{login_err_msg}</Alert> : null}
                    <TextField helperText={err_msg} error={error} label='Email' style={{ width: '100%', }} onChange={event => { handleEmailChange(event) }}></TextField>
                    <TextField helperText={err_msg_pass} error={error_pass} label='Password' type='password' style={{ width: '100%', }} onChange={event => { handlePassChange(event) }}></TextField>
                    <div style={{ position: 'relative' }}>
                        <Button type='submit' disabled={loading || validated_email(email).error || validated_pass(password).error} variant='contained' color='primary' onClick={handleClick} style={{ width: '100%', marginTop: '15px' }}>
                            Login
                    </Button>
                        {loading ? <CircularProgress size={30} style={{ position: 'absolute', top: '18px', right: 0, left: 0, margin: 'auto' }} /> : null}
                    </div>
                </div>
            </Grid>
        </Grid>
    )
}

