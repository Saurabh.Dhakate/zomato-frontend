import React from 'react'
import SearchResult from './SearchResult'

export default function SearchResults() {
    
    return (
        <div style={{
            width: '96%',
            maxHeight: '325px',
            backgroundColor: 'white',
            boxShadow: 'grey 2px 2px 10px',
            margin: '-25px 0',
            position: 'relative',
            zIndex: 1,
            padding: '30px 2%',
            borderRadius: '10px',
            overflowY: 'scroll'
        }}>

            <SearchResult name='Choca Lava Cake' price='98' restaurant='Dominos' image='https://i2-prod.belfastlive.co.uk/incoming/article12963211.ece/ALTERNATES/s1200c/Dominos-Pizza.jpg' />
            <SearchResult name='Hot Coffee' price='156' restaurant='Cafe Coffee Day' image='https://www.deccanherald.com/sites/dh/files/article_images/2020/05/19/059674-01-02-273920429-1578913886.jpg' />
            <SearchResult name='Pizza' price='204' restaurant='Pizza Hut' image='https://images.unsplash.com/photo-1592977731761-2d58aafef572?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=564&q=80' />
            <SearchResult name='Burger' price='54' restaurant="McDonald's" image='https://media-cdn.tripadvisor.com/media/photo-s/07/dd/45/e1/mcdonald-s.jpg' />
            <SearchResult name='Hot Wings' price='499' restaurant='KFC' image='https://www.joc.com/sites/default/files/field_feature_image/KFC_0.png' />
        </div>
    )
}
