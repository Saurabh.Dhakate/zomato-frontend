import { Button, Card } from '@material-ui/core'
import React from 'react'
import { useHistory } from 'react-router-dom'

export default function SearchResult(props) {
    let history = useHistory()
    return (
        <Card style={{margin:'10px 0'}}>
            <div style={{ width: '100%', height: '100px', display: 'flex', justifyContent: 'space-between' }}>
                <div style={{
                    minWidth: '100px',
                    height: '100px',
                    backgroundSize: '100px 100%',
                    backgroundImage: `url(${props.image})`
                }}>
                </div>

                <div style={{ minWidth: '10px', marginLeft: '5%', marginRight: 'auto', display: 'flex', flexDirection: "column", justifyContent: 'space-evenly' }}>
                    <h3 style={{ margin: '0' }}>{props.name}</h3>
                    <h5 style={{ margin: '0', color: 'grey' }}>Price - ₹ {props.price}</h5>
                    <h5 style={{ margin: '0' }}>{props.restaurant}</h5>
                </div>
                <div className='btn-grp' style={{ display: 'flex', flexDirection: 'row', height: '100px', justifyContent: 'space-evenly', flexWrap: 'wrap', alignContent: 'space-around' }}>
                    <Button size='small' style={{ height: 'min-content', width: '115px' }} color="primary" variant='contained' onClick={()=>history.push(`/restaurant/${props.restaurant}`)}>Order Online</Button>
                    <Button size='small' style={{ height: 'min-content', width: '115px' }} color='secondary' variant='contained'>Dine Out</Button>
                </div>
            </div>
        </Card>
    )
}
