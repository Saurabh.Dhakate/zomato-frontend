import React from 'react'
import { Card, CardMedia, Grid } from '@material-ui/core'
import { Link } from 'react-router-dom'
import SearchBox from './SearchBox'
import Account from './Account'

export default function Homepage() {

    return (
        <>
            <Grid container justify='center' style={{
                height: '450px',
                backgroundImage: 'linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ),url(https://images.squarespace-cdn.com/content/v1/5c5c3833840b161566b02a76/1573133725500-Y5PCN0V04I86HDAT8AT0/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/WBC_7095.jpg?format=2500w)',
                backgroundSize: 'cover',
                backgroundPosition: '0% 50%'
            }} alignItems='center'>
                <Account />
                <Grid item md={8} xs={11} style={{ height: '80px' }}>
                    <h1 className='home-brand' style={{ margin: '20px 0', textAlign: 'center', color: 'white', fontFamily: 'Lobster' }}>Good - Food</h1>
                    <h5 style={{ margin: '10px 0', textAlign: 'center', color: 'white', fontSize: '20px', fontFamily: 'Philosopher' }}>To live a full life, you have to fill your stomach first.</h5>
                </Grid>
                <Grid item md={8} xs={11} style={{ height: '56px' }}>
                    <SearchBox />
                </Grid>
            </Grid>
            <Grid container justify='space-around' alignItems='center' style={{ height: '300px', backgroundColor: '#cecece' }}>
                <Grid item md={4} xs={5}>
                    <Link to='/restaurant' style={{ textDecoration: 'none' }}>
                        <Card style={{ width: '100%' }}>
                            <CardMedia
                                style={{
                                    height: '150px',
                                    backgroundImage: "url(https://www.apptunix.com/blog/wp-content/uploads/sites/3/2020/04/online-food-delivery-industry.jpg)",
                                    backgroundSize: 'center'
                                }}>
                            </CardMedia>
                            <h4 style={{ textAlign: 'center' }}>Order Online</h4>
                        </Card></Link>
                </Grid>
                <Grid item md={4} xs={5}>
                    <Link to='/restaurant' style={{ textDecoration: 'none' }}>
                        <Card style={{ width: '100%' }}>
                            <CardMedia
                                style={{
                                    height: '150px',
                                    backgroundImage: "url(https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8cmVzdGF1cmFudHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80)",
                                    backgroundSize: 'center'
                                }}>
                            </CardMedia>
                            <h4 style={{ textAlign: 'center' }}>Dine Out</h4>
                        </Card></Link>
                </Grid>
            </Grid>
            <Grid container>

            </Grid>
        </>
    )
}

