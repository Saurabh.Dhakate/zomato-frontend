import React, { useState } from 'react'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import { Fab } from '@material-ui/core'
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser'
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { useHistory } from 'react-router-dom'

export default function Account() {
    let [signOut, setSignOut] = useState(false)
    let history = useHistory()
    let loggedOut = () => (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <Fab color='primary' variant='extended' onClick={() => { setSignOut(!signOut) }}>
                <VerifiedUserIcon style={{ marginRight: '5px' }} />
                Hey, {localStorage.getItem('user')}
            </Fab>
            {(signOut) ? <Fab color='secondary' variant='extended' style={{ margin: '10px 0' }} onClick={() => { localStorage.removeItem('user'); history.go(0)}}>
                <ExitToAppIcon style={{ marginRight: '10px' }} />
                Signout
            </Fab> : null}

        </div>
    )
    let loggedIn = () => (
        <Fab color="white" variant='extended' onClick={() => { localStorage.setItem('login_from', `${window.location}`); history.push('/user') }}>
            <AccountCircleIcon style={{ marginRight: '5px' }} />
            Signup / Login
        </Fab>

    )
    return (
        <><div style={{ position: 'absolute', right: '2.5%', top: '20px', color: 'white', width: 'max-content', zIndex: '1' }}>
            {(localStorage.getItem('user')) ? loggedOut() : loggedIn()}
        </div>
        </>
    )
}
