import { TextField } from '@material-ui/core'
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import SearchResult from './SearchResults'

export default function SearchBox() {
    const dispatch = useDispatch()
    let searchFood = useSelector(state => state.home.searchResult)

    let handleSearchChange = e => {
        dispatch({ type: 'update/searchResult', payload: e.target.value })
    }
    return (
        <>
            <TextField fullWidth placeholder="Search Your Favourite Food" variant="outlined"
                InputProps={{
                    style: {
                        backgroundColor: 'white',
                        borderRadius: '20px',
                        position: 'relative',
                        zIndex: 2,
                        boxShadow: 'black 0px 5px 20px'
                    }
                }} onChange={e => handleSearchChange(e)} />
            {(searchFood.length <= 2) ? null : <SearchResult />}
        </>
    )
}
