import React from 'react';
import { Grid } from '@material-ui/core'
import SearchBox from '../Landing/SearchBox';
import Account from '../Landing/Account';

export default function Header() {
  return (
    <Grid justify="center" alignItems="center" container item lg={8} style={{ height: '100px' }}>
      <Grid item container lg={12} justify="space-around">
        <Grid item style={{ fontSize: '1.8em', marginTop: '20px', fontWeight: 'bold', fontFamily:'Lobster' }}>
          <span>GOOD-FOOD</span>
        </Grid>
        <Grid lg={8} item>
          <SearchBox />
        </Grid>

        <Account />
      </Grid>
    </Grid>
  );
}
