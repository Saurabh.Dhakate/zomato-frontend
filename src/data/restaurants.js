module.exports = [
    {
      "restaurant": {
        "R": {
          "res_id": 10125,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": true,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "10125",
        "name": "Vaishali",
        "url": "https://www.zomato.com/pune/vaishali-fc-road?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "1218/1, Rage Path, Ganeshwadi, Shivajinagar, FC Road, Pune",
          "locality": "FC Road",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.5209714548",
          "longitude": "73.8411796466",
          "zipcode": "0",
          "country_id": 1,
          "locality_verbose": "FC Road, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "South Indian, Sandwich, Fast Food, Beverages",
        "timings": "7 AM to 11 PM",
        "average_cost_for_two": 500,
        "price_range": 2,
        "currency": "Rs.",
        "highlights": [
          "Delivery",
          "No Alcohol Available",
          "Cash",
          "Lunch",
          "Credit Card",
          "Dinner",
          "Breakfast",
          "Takeaway Available",
          "Debit Card",
          "Indoor Seating",
          "Outdoor Seating",
          "Pure Veg",
          "Table Reservation Not Required"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/5/10125/9b82ae2f1aecabc6af5bd66cc97641a2.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.5",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "rating_obj": {
            "title": {
              "text": "4.5"
            },
            "bg_color": {
              "type": "lime",
              "tint": "700"
            }
          },
          "votes": 18139
        },
        "all_reviews_count": 2793,
        "photos_url": "https://www.zomato.com/pune/vaishali-fc-road/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 2166,
        "menu_url": "https://www.zomato.com/pune/vaishali-fc-road/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/5/10125/9b82ae2f1aecabc6af5bd66cc97641a2.jpg",
        "medio_provider": false,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/10125",
        "order_url": "https://www.zomato.com/pune/vaishali-fc-road/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/vaishali-fc-road/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "020 25531244",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Casual Dining"
        ]
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 10007,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": true,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "10007",
        "name": "Blue Nile",
        "url": "https://www.zomato.com/pune/blue-nile-camp-area?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "4, Camp Area, Pune",
          "locality": "Camp Area",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.5218415758",
          "longitude": "73.8774504885",
          "zipcode": "0",
          "country_id": 1,
          "locality_verbose": "Camp Area, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "North Indian, Chinese, Mughlai, Malwani, Desserts",
        "timings": "11:30 AM to 11 PM",
        "average_cost_for_two": 800,
        "price_range": 2,
        "currency": "Rs.",
        "highlights": [
          "Dinner",
          "Cash",
          "Takeaway Available",
          "Debit Card",
          "Lunch",
          "Delivery",
          "Credit Card",
          "Indoor Seating",
          "Air Conditioned"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/7/10007/97eb9ac98ce6ffbe25d7ccee4e36c83a.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.1",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "rating_obj": {
            "title": {
              "text": "4.1"
            },
            "bg_color": {
              "type": "lime",
              "tint": "700"
            }
          },
          "votes": 6071
        },
        "all_reviews_count": 1508,
        "photos_url": "https://www.zomato.com/pune/blue-nile-camp-area/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 934,
        "menu_url": "https://www.zomato.com/pune/blue-nile-camp-area/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/7/10007/97eb9ac98ce6ffbe25d7ccee4e36c83a.jpg?output-format=webp",
        "medio_provider": false,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/10007",
        "order_url": "https://www.zomato.com/pune/blue-nile-camp-area/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/blue-nile-camp-area/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "020 26125238",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Casual Dining"
        ]
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 12036,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": true,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "12036",
        "name": "SP's Biryani House",
        "url": "https://www.zomato.com/pune/sps-biryani-house-sadashiv-peth?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "1472, Sadashiv Peth, Pune",
          "locality": "Sadashiv Peth",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.5082507140",
          "longitude": "73.8503189385",
          "zipcode": "0",
          "country_id": 1,
          "locality_verbose": "Sadashiv Peth, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Biryani, Mughlai",
        "timings": "11 AM to 3 PM, 7 PM to 11 PM",
        "average_cost_for_two": 650,
        "price_range": 2,
        "currency": "Rs.",
        "highlights": [
          "Dinner",
          "Cash",
          "Takeaway Available",
          "Debit Card",
          "Lunch",
          "Delivery",
          "Credit Card",
          "Indoor Seating",
          "Air Conditioned"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/6/12036/0e5279b7e2b8a3e17dbb4fa2826bda8a.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.2",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "rating_obj": {
            "title": {
              "text": "4.2"
            },
            "bg_color": {
              "type": "lime",
              "tint": "700"
            }
          },
          "votes": 4283
        },
        "all_reviews_count": 1020,
        "photos_url": "https://www.zomato.com/pune/sps-biryani-house-sadashiv-peth/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 450,
        "menu_url": "https://www.zomato.com/pune/sps-biryani-house-sadashiv-peth/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/6/12036/0e5279b7e2b8a3e17dbb4fa2826bda8a.jpg",
        "medio_provider": false,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/12036",
        "order_url": "https://www.zomato.com/pune/sps-biryani-house-sadashiv-peth/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/sps-biryani-house-sadashiv-peth/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "020 24475441, 020 24432117",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Casual Dining"
        ]
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 13691,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": -1,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "13691",
        "name": "Madinah Restaurant",
        "url": "https://www.zomato.com/pune/madinah-restaurant-kondhwa?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "Near Kausar Baug Masjid, Kondhwa, Pune",
          "locality": "Kondhwa",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.4720932418",
          "longitude": "73.8934441656",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Kondhwa, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "North Indian, Chinese, Mughlai",
        "timings": "12noon – 11pm (Mon-Sun)",
        "average_cost_for_two": 400,
        "price_range": 1,
        "currency": "Rs.",
        "highlights": [
          "Dinner",
          "Delivery",
          "Credit Card",
          "Lunch",
          "Cash",
          "Takeaway Available",
          "Debit Card",
          "Outdoor Seating",
          "Indoor Seating",
          "Sodexo"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/9/19041569/51d0cd9e9664b59cb5e109104452ed46.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.0",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "rating_obj": {
            "title": {
              "text": "4.0"
            },
            "bg_color": {
              "type": "lime",
              "tint": "600"
            }
          },
          "votes": 25132
        },
        "all_reviews_count": 1246,
        "photos_url": "https://www.zomato.com/pune/madinah-restaurant-kondhwa/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 203,
        "menu_url": "https://www.zomato.com/pune/madinah-restaurant-kondhwa/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/9/19041569/51d0cd9e9664b59cb5e109104452ed46.jpg",
        "medio_provider": false,
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/13691",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/madinah-restaurant-kondhwa/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "+91 8928385828",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Casual Dining"
        ]
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 12009,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": -1,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "12009",
        "name": "Kayani Bakery",
        "url": "https://www.zomato.com/pune/kayani-bakery-east-street?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "6, DEH, Koyani Road, Camp, East Street, Pune",
          "locality": "East Street",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.5148629976",
          "longitude": "73.8798309490",
          "zipcode": "0",
          "country_id": 1,
          "locality_verbose": "East Street, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Bakery",
        "timings": "7:30 AM to 1 PM, 3:30 PM to 8 PM (Mon-Sat), Sun Closed",
        "average_cost_for_two": 150,
        "price_range": 1,
        "currency": "Rs.",
        "highlights": [
          "No Seating Available",
          "Delivery",
          "Cash",
          "Takeaway Available",
          "Desserts and Bakes"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/res_imagery/12009_CHAIN_92f213d260750cef6aab91fd000fb5b2.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.5",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "rating_obj": {
            "title": {
              "text": "4.5"
            },
            "bg_color": {
              "type": "lime",
              "tint": "700"
            }
          },
          "votes": 3804
        },
        "all_reviews_count": 976,
        "photos_url": "https://www.zomato.com/pune/kayani-bakery-east-street/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 323,
        "menu_url": "https://www.zomato.com/pune/kayani-bakery-east-street/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/res_imagery/12009_CHAIN_92f213d260750cef6aab91fd000fb5b2.jpg",
        "medio_provider": false,
        "has_online_delivery": 1,
        "is_delivering_now": 0,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/12009",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/kayani-bakery-east-street/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "020 26360517",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Bakery"
        ]
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18461817,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": true,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "18461817",
        "name": "P.K. Dum Biryani House",
        "url": "https://www.zomato.com/pune/p-k-dum-biryani-house-1-sinhgad-road?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "Shop A-1, Survey 54, Near Navale Birdge, Harshawardhan Pride, Sinhgad Road, Pune",
          "locality": "Sinhgad Road",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.4617661321",
          "longitude": "73.8217115030",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Sinhgad Road, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "North Indian, Mughlai, Biryani",
        "timings": "11 AM to 3:30 PM, 7 PM to 11 PM",
        "average_cost_for_two": 550,
        "price_range": 2,
        "currency": "Rs.",
        "highlights": [
          "Dinner",
          "Credit Card",
          "Cash",
          "Debit Card",
          "Delivery",
          "Lunch",
          "Takeaway Available",
          "Indoor Seating",
          "Free Parking"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/7/18461817/9e33bdbaf3b01b5c5efee20bbeb524c8.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.0",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "rating_obj": {
            "title": {
              "text": "4.0"
            },
            "bg_color": {
              "type": "lime",
              "tint": "600"
            }
          },
          "votes": 2690
        },
        "all_reviews_count": 100,
        "photos_url": "https://www.zomato.com/pune/p-k-dum-biryani-house-1-sinhgad-road/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 35,
        "menu_url": "https://www.zomato.com/pune/p-k-dum-biryani-house-1-sinhgad-road/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/7/18461817/9e33bdbaf3b01b5c5efee20bbeb524c8.jpg",
        "medio_provider": false,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18461817",
        "order_url": "https://www.zomato.com/pune/p-k-dum-biryani-house-1-sinhgad-road/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/p-k-dum-biryani-house-1-sinhgad-road/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "+91 9764141773, +91 9881093640",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Quick Bites"
        ]
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 13231,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": true,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "13231",
        "name": "Le Plaisir",
        "url": "https://www.zomato.com/pune/le-plaisir-deccan-gymkhana?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "Survey 759/125, Rajkamal, Opposite Kelkar Eye Hospital, Prabhat Road, Deccan Gymkhana, Pune",
          "locality": "Deccan Gymkhana",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.5142538566",
          "longitude": "73.8386224955",
          "zipcode": "0",
          "country_id": 1,
          "locality_verbose": "Deccan Gymkhana, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Cafe, Italian, Continental, European",
        "timings": "9am – 11pm (Mon-Sun)",
        "average_cost_for_two": 1000,
        "price_range": 3,
        "currency": "Rs.",
        "highlights": [
          "Dinner",
          "Delivery",
          "Credit Card",
          "Debit Card",
          "Lunch",
          "Cash",
          "Takeaway Available",
          "Indoor Seating",
          "Gourmet",
          "Air Conditioned",
          "All Day Breakfast",
          "Desserts and Bakes"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/1/13231/1ca3195a76a58ba11803966cb1dc32c8.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.6",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "rating_obj": {
            "title": {
              "text": "4.6"
            },
            "bg_color": {
              "type": "lime",
              "tint": "800"
            }
          },
          "votes": 7956
        },
        "all_reviews_count": 2574,
        "photos_url": "https://www.zomato.com/pune/le-plaisir-deccan-gymkhana/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 7260,
        "menu_url": "https://www.zomato.com/pune/le-plaisir-deccan-gymkhana/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/1/13231/1ca3195a76a58ba11803966cb1dc32c8.jpg",
        "medio_provider": false,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/13231",
        "order_url": "https://www.zomato.com/pune/le-plaisir-deccan-gymkhana/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/le-plaisir-deccan-gymkhana/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "+91 8550995835, +91 8550995865",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Café"
        ]
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 6503864,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": true,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "6503864",
        "name": "PK Biryani House",
        "url": "https://www.zomato.com/pune/pk-biryani-house-karve-nagar?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "Rajaram Bridge, Tathawade Garden, 100 D.P. Road, Karve Nagar, Pune",
          "locality": "Karve Nagar",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.4925325599",
          "longitude": "73.8285541534",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Karve Nagar, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Biryani, North Indian, Seafood",
        "timings": "11 AM to 3 PM, 7 PM to 11 PM",
        "average_cost_for_two": 700,
        "price_range": 2,
        "currency": "Rs.",
        "highlights": [
          "Dinner",
          "Cash",
          "Takeaway Available",
          "Debit Card",
          "Lunch",
          "Delivery",
          "Credit Card",
          "Indoor Seating",
          "Table booking recommended",
          "Digital Payments Accepted"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/4/6503864/3030403348cd9adc57f737ee90ed1cf2.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.1",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "rating_obj": {
            "title": {
              "text": "4.1"
            },
            "bg_color": {
              "type": "lime",
              "tint": "700"
            }
          },
          "votes": 31249
        },
        "all_reviews_count": 918,
        "photos_url": "https://www.zomato.com/pune/pk-biryani-house-karve-nagar/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 130,
        "menu_url": "https://www.zomato.com/pune/pk-biryani-house-karve-nagar/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/4/6503864/3030403348cd9adc57f737ee90ed1cf2.jpg",
        "medio_provider": false,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/6503864",
        "order_url": "https://www.zomato.com/pune/pk-biryani-house-karve-nagar/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/pk-biryani-house-karve-nagar/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "+91 9579423316, +91 9822026463",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Casual Dining"
        ]
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18569092,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": true,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "18569092",
        "name": "Barometer",
        "url": "https://www.zomato.com/pune/barometer-kothrud?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "Shop 2, Chintamani Pride, City Pride Road, Paschimanagri, Kothrud, Pune",
          "locality": "Kothrud",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.4988071494",
          "longitude": "73.8192341477",
          "zipcode": "411038",
          "country_id": 1,
          "locality_verbose": "Kothrud, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, Asian, North Indian, Finger Food",
        "timings": "8 AM to 11 PM",
        "average_cost_for_two": 1500,
        "price_range": 3,
        "currency": "Rs.",
        "highlights": [
          "Dinner",
          "Delivery",
          "Debit Card",
          "Takeaway Available",
          "Cash",
          "Credit Card",
          "Lunch",
          "Breakfast",
          "Live Sports Screening",
          "Fullbar",
          "Wifi",
          "Indoor Seating",
          "Air Conditioned",
          "Gourmet"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/chains/2/18569092/85f7f076de3511568de81549ca524f6c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.6",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "rating_obj": {
            "title": {
              "text": "4.6"
            },
            "bg_color": {
              "type": "lime",
              "tint": "800"
            }
          },
          "votes": 4188
        },
        "all_reviews_count": 783,
        "photos_url": "https://www.zomato.com/pune/barometer-kothrud/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 1541,
        "menu_url": "https://www.zomato.com/pune/barometer-kothrud/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/chains/2/18569092/85f7f076de3511568de81549ca524f6c.jpg",
        "medio_provider": false,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18569092",
        "order_url": "https://www.zomato.com/pune/barometer-kothrud/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/barometer-kothrud/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "+91 9112271000",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Casual Dining"
        ]
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 10140,
          "is_grocery_store": false,
          "has_menu_status": {
            "delivery": true,
            "takeaway": -1
          }
        },
        "apikey": "8ff1e1bddc19d2ef9a5254d6ae9d5b50",
        "id": "10140",
        "name": "Tiranga Bhuvan Kothrud",
        "url": "https://www.zomato.com/pune/tiranga-bhuvan-kothrud-kothrud?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "Pitambar Heights, Paud Phata Road, Near Paud Phata Fly Over, Kothrud, Pune",
          "locality": "Kothrud",
          "city": "Pune",
          "city_id": 5,
          "latitude": "18.5071398457",
          "longitude": "73.8234076649",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Kothrud, Pune"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Maharashtrian, North Indian, Biryani",
        "timings": "11:30am – 3:30pm, 7pm – 11pm (Mon-Sat),11:30am – 3:30pm, 7pm – 11:30pm (Sun)",
        "average_cost_for_two": 1100,
        "price_range": 3,
        "currency": "Rs.",
        "highlights": [
          "Lunch",
          "Delivery",
          "Credit Card",
          "No Alcohol Available",
          "Dinner",
          "Cash",
          "Takeaway Available",
          "Debit Card",
          "Indoor Seating",
          "Table Reservation Not Required"
        ],
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/0/10140/b28dbfe49a4e73c096a4f2c70b249713.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.3",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "rating_obj": {
            "title": {
              "text": "4.3"
            },
            "bg_color": {
              "type": "lime",
              "tint": "700"
            }
          },
          "votes": 4187
        },
        "all_reviews_count": 632,
        "photos_url": "https://www.zomato.com/pune/tiranga-bhuvan-kothrud-kothrud/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "photo_count": 302,
        "menu_url": "https://www.zomato.com/pune/tiranga-bhuvan-kothrud-kothrud/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/0/10140/b28dbfe49a4e73c096a4f2c70b249713.jpg",
        "medio_provider": false,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "store_type": "",
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/10140",
        "order_url": "https://www.zomato.com/pune/tiranga-bhuvan-kothrud-kothrud/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/pune/tiranga-bhuvan-kothrud-kothrud/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "phone_numbers": "+91 9881025552",
        "all_reviews": {
          "reviews": [
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            },
            {
              "review": []
            }
          ]
        },
        "establishment": [
          "Casual Dining"
        ]
      }
    }
  ]