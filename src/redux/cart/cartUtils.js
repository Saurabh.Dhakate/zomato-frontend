const addItemToCart = (cartItems, itemToAdd) => {
    const existingItem = cartItems.find((cartItem)=>{
        return cartItem.id === itemToAdd.id
    })

    if(existingItem){
        return cartItems.map((cartItem)=>{
            if(cartItem.id === itemToAdd.id){
                return {...cartItem, quantity: cartItem.quantity + 1}
            }
            return cartItem
        })
    }

    return [...cartItems, {...itemToAdd, quantity: 1}]
}

export const removeItemFromCart = (cartItems, itemToRemove) => {

    const existingItem = cartItems.find((cartItem)=>{
        return cartItem.id === itemToRemove.id
    })

    if(existingItem){
        if(existingItem.quantity > 1){
            return cartItems.map((cartItem)=>{
                if(cartItem.id === existingItem.id){
                    return {...cartItem, quantity: cartItem.quantity - 1}
                }
                return cartItem
            })
        }else{
            return cartItems.filter((cartItem)=>{
                
                return cartItem.id !== existingItem.id
            })
        }
    }else{
        return []
    }

    
}

export default addItemToCart