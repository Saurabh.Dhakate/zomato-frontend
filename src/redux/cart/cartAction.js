const toggleCartDropdown = () => {
    return {
        type: 'TOGGLE_CART_VISIBILITY'
    }
}

export const addItemToCart = (item) => {
    console.log(item,"kikik")
    return {
        type:'ADD_TO_CART',
        payload: item
    }
}

export const removeItemFromCart = (item) => {
    return {
        type:'REMOVE_ITEM_FROM_CART',
        payload: item
    }
}

export default toggleCartDropdown