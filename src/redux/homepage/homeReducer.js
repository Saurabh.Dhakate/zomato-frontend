const initial_state = {
    searchResult: ""
}

const reducer = (state = initial_state, action) => {
    switch (action.type) {
        case 'update/searchResult':
            return {
                ...state,
                searchResult: action.payload
            }
        default: return state
    }
}

export default reducer