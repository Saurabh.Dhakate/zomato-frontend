const initial_state = {
    userOtp: '',
    otp_err: null
}

let reducer = (state = initial_state, action) => {
    switch (action.type) {
        case 'update/userOtp':
            return {
                ...state,
                userOtp: action.userOtp,
                otp_err: null
            }
        case 'update/otp_err':
            return {
                ...state,
                otp_err: action.otp_err
            }
        default: return state
    }
}

export default reducer