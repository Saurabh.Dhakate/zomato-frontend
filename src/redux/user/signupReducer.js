const initial_state = {
    error: false,
    error_pass: false,
    error_repass: false,
    error_fname: false,
    err_msg_pass: '',
    err_msg_repass: '',
    err_msg_fname: '',
    email: '',
    err_msg: '',
    password: '',
    repassword: '',
    fname: '',
    otp: '',
    signup_err_msg: null

}

const reducer = (state = initial_state, action) => {
    switch (action.type) {
        case 'update/email':
            return {
                ...state,
                email: action.payload,
                error: action.error,
                err_msg: action.err_msg,
                signup_err_msg:null
            }
        case 'update/pass':
            return {
                ...state,
                password: action.payload,
                error_pass: action.error_pass,
                err_msg_pass: action.err_msg_pass
            }
        case 'update/repass':
            return {
                ...state,
                repassword: action.repassword,
                error_repass: action.error_repass,
                err_msg_repass: action.err_msg_repass
            }
        case 'update/fname':
            return {
                ...state,
                fname: action.fname,
                error_fname: action.error_fname,
                err_msg_fname: action.err_msg_fname
            }
        case 'update/lname':
            return {
                ...state,
                lname: action.lname,
                error_lname: action.error_lname,
                err_msg_lname: action.err_msg_lname
            }
        case 'update/otp':
            return {
                ...state,
                otp: action.otp
            }
        case 'update/signup_err':
            return {
                ...state,
                signup_err_msg: action.signup_err_msg
            }
        default: return state
    }
}

export default reducer