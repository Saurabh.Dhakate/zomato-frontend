const initial_state = {
    error: false,
    error_pass: false,
    err_msg_pass: '',
    email: '',
    err_msg: '',
    password: '',
    login_msg: null,
    login_err_msg:null
}

const reducer = (state = initial_state, action) => {
    switch (action.type) {
        case 'update/email':
            return {
                ...state,
                email: action.payload,
                error: action.error,
                err_msg: action.err_msg
            }
        case 'update/pass':
            return {
                ...state,
                password: action.payload,
                error_pass: action.error_pass,
                err_msg_pass: action.err_msg_pass
            }
        case 'loggedin':
            return {
                ...state,
                login_msg:action.payload.login_msg,
                login_err_msg:action.payload.login_err_msg
            }
        default: return state
    }
}

export default reducer