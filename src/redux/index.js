import loginReducer from './user/loginReducer'
import signupReducer from './user/signupReducer'
import otpReducer from './user/otpReducer'
import cartReducer from './cart/cartReducer'
import homeReducer from './homepage/homeReducer'

import {createStore,combineReducers} from 'redux'

const rootReducer = combineReducers({
    login:loginReducer,
    signup:signupReducer,
    otp:otpReducer,
    cart:cartReducer,
    home:homeReducer
})

export default  createStore(rootReducer)